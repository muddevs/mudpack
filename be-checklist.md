# Launch Checklist (back-end)
Project: [INSERT HERE]
Server IP: [INSERT HERE]

## Pre-launch

- [ ] robots.txt
	- [ ] ensure live is excluding admin only
	- [ ] staging is disallow all
- [ ] SEO plugin
	- [ ] Sections added to sitemap
	- [ ] if custom template this is set in plugin settings
	- [ ] social app added if required
	- [ ] Check Site Name is correct
	- [ ] If change, ensure (ether) social image titles are up-to-date. If not Find and Replace
	- [ ] Redirects set with asset ID available across environments (local, staging and live)
- [ ] SEO plugin fields
	- [ ] fallback images defined for social media
		- [ ] [Sharing Debugger - Facebook for Developers](https://developers.facebook.com/tools/debug/)
		- [ ] https://cards-dev.twitter.com/validator
		- [ ] https://metatags.io/ (note that robots.txt will prevent scans for non-live enviroments)
- [ ] Tracking
	- [ ] site added to Google Tag Manager
	- [ ] Analytics added to GTM
	- [ ] Publish updates
- [ ] Forms testing
	- [ ] forms being submitted
	- [ ] useful validation returned if errors
	- [ ] useful response if success
	- [ ] emails being delivered
	- [ ] if assets sent these are saved and accessible
- [ ] Emails
	- [ ] Mandrill:
		- [ ] staging use mandrill > config for staging/mudbank SMTP
		- [ ] mandrill set-up if required for email delivery
	- [ ] system email is a client email
- [ ] Caching enabled for live
	- [ ] element api
	- [ ] templates
	- [ ] mudmodule
	- [ ] Blitz
- [ ] Image transforms in place (front-end ???)
- [ ] Deprecation errors checked in Utilities
	- [ ] Blitz recommendations
- [ ] Eager loading checked ???
	- [ ] [GitHub - putyourlightson/craft-elements-panel: Adds an elements panel to the Craft CMS debug toolbar.](https://github.com/putyourlightson/craft-elements-panel)
- [ ] Optimised htaccess
	- [ ] GZIP compression
	- [ ] expiry headers
	- [ ] CROSS-ORIGIN headers
	- [ ] TO DO: Cole to add from i-coach experiences
- [ ] check clock in CMS
	- [ ] TO DO: Cole to check possible in config?
- [ ] env files
	- [ ] DEV_MODE = 0
	- [ ] ALLOW_ADMIN_CHANGES = 0
	- [ ] IS_SYSTEM_LIVE = 1
	- [ ] ENVIRONMENT = "LIVE"
	- [ ] update domain
	- [ ] check/double-check env file protocol is correct (eg https not http)
- [ ] SSL
	- [ ] if server pilot PRO then ensure HTTPS and HTTP > HTTPS redirect
	- [ ] if hosting certificate added [eg via Lets Encrypt](https://github.com/lesaff/serverpilot-letsencrypt)
		- [ ] CRON job set up for certificate renewal
- [ ] Pub
	- [ ] 🍻

## Post-launch

- [ ] sitemap added to Google Search Console
- [ ] password added to cipher
- [ ] site added to hosting spreadsheet tab
- [ ] license(s) noted in hosting spreadsheet tab
- [ ] site URL added to notes for CMS and plugin licenses (id.craftcms.com)
- [ ] site added to uptime robot
- [ ] server monitoring added in Digital Ocean (post to Slack for CPU, Memory Utilisation & Disk Utilisation is running high)