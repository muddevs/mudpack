const gulp = require('gulp');
const browserSync = require('browser-sync');
const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const htmlreplace = require('gulp-html-replace');
const middleware = require('../webpack/middleware');
const devConfig = require('../webpack/config.dev');

const {
	PATHS: {
		cacheTag: { files, dir },
	},
} = global;

const clearCacheTags = () => {
	function clearCacheTagsPromise(url) {
		return gulp
			.src(url)
			.pipe(
				htmlreplace(
					{
						cms: {
							src: '',
							tpl: '{% set stamp = "%s" %}',
						},
					},
					{
						keepBlockTags: true,
					}
				)
			)
			.pipe(gulp.dest(path.resolve(process.env.PWD, dir)));
	}

	return files.reduce(async (previousPromise, nextPath) => {
		await previousPromise;
		return clearCacheTagsPromise(path.resolve(process.env.PWD, dir, nextPath));
	}, Promise.resolve());
};

const cacheTags = () => {
	function cacheTagsPromise(url) {
		return gulp
			.src(url)
			.pipe(
				htmlreplace(
					{
						cms: {
							src: PRODUCTION ? `.${global.TASK.stamp}` : '',
							tpl: '{% set stamp = "%s" %}',
						},
					},
					{
						keepBlockTags: true,
					}
				)
			)
			.pipe(gulp.dest(path.resolve(process.env.PWD, dir)));
	}

	return files.reduce(async (previousPromise, nextPath) => {
		await previousPromise;
		return cacheTagsPromise(path.resolve(process.env.PWD, dir, nextPath));
	}, Promise.resolve());
};

const serverProxy = done => {
	const compiler = webpack(merge(global.WEBPACK_CONFIG, devConfig));

	const {
		PATHS: { proxy },
		TASK: { server },
	} = global;

	browserSync.init({
		...middleware(compiler),
		proxy,
		...server,
	});

	done();
};

module.exports = {
	serverProxy,
	clearCacheTags,
	cacheTags,
};
