import domify from 'domify';

/** *
 * @memberof dom
 * @function insertBefore
 *
 * @description Insert a dom node before another node
 *
 * @property {HTMLElement | String}  node - the node to insert
 * @property {HTMLelement} before - the node to insert before
 *
 * @return {void}
 *
 */

export function insertBefore(node, before) {
	const elm = typeof node === 'string' ? domify(node) : node;

	return before.parentNode.insertBefore(elm, before);
}

/** *
 * @memberof dom
 * @function insertAfter
 *
 * @description Insert a dom node before another node
 *
 * @property {HTMLElement | String}  node - the node to insert
 * @property {HTMLelement} before - the node to insert after
 *
 * @return {void}
 *
 */

export function insertAfter(node, after) {
	const elm = typeof node === 'string' ? domify(node) : node;
	return after.parentNode.insertBefore(elm, after.nextSibling);
}

/**
 * @module utils/renderInTheLoop
 */

/**
 * @function renderInTheLoop
 * @description fun with the event loop
 * @see https://www.youtube.com/watch?v=8aGhZQkoFbQ
 * @see https://www.youtube.com/watch?v=cCOL7MC4Pl0
 * @param {function} callback
 * @return {void}
 */
export function renderInTheLoop(callback) {
	requestAnimationFrame(() => {
		requestAnimationFrame(() => callback());
	});
}

/**
 * @module utils/getIdFromHref
 */

/**
 * @function getIdFromHref
 * @param {HTMLElement} node
 * @return {string}
 */
export function getIdFromHref(node) {
	/**
	 * get the href from the button
	 *
	 * @private
	 * @type {String}
	 */
	const targetSelector = node.getAttribute('href');

	/**
	 * remove the hash
	 *
	 * @private
	 * @type {String}
	 */
	const id = targetSelector.split('#').pop();

	return id;
}

const eventCache = {};

export function getEventName(event) {
	if (event !== 'transitionend' || event !== 'animationend') return event;

	if (eventCache[event]) return eventCache[event];

	const types =
		event === 'transitionend'
			? {
					OTransition: 'oTransitionEnd',
					WebkitTransition: 'webkitTransitionEnd',
					MozTransition: 'transitionend',
					transition: 'transitionend',
			  }
			: {
					OAnimation: 'oAnimationEnd',
					WebkitAnimation: 'webkitAnimationEnd',
					MozAnimation: 'animationend',
					animation: 'animationend',
			  };

	const elem = document.createElement('div');

	const name = Object.keys(types).reduce(
		(prev, trans) => (undefined !== elem.style[trans] ? types[trans] : prev),
		''
	);

	eventCache[event] = name;

	return name;
}

export function debounce(func, wait, immediate) {
	let timeout;
	return (...args) => {
		const context = this;
		const later = () => {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		const callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

export function delay(delay = 1000) {
	return new Promise(resolve => setTimeout(resolve, delay));
}

export function eventPromise(event, element, callback) {
	let complete = false;

	const done = (resolve, e) => {
		e.stopPropagation();
		element.removeEventListener(event, done);
		if (e.target === element && !complete) {
			complete = true;
			resolve();
		}
	};

	return new Promise(resolve => {
		callback();
		element.addEventListener(event, done.bind(null, resolve));
	});
}

export function random(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}
