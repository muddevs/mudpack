<?php
/**
 * SEOmatic plugin for Craft CMS 3.x
 *
 * A turnkey SEO implementation for Craft CMS that is comprehensive, powerful,
 * and flexible
 *
 * @link      https://nystudio107.com
 * @copyright Copyright (c) 2017 nystudio107
 */

/**
 * @author    nystudio107
 * @package   Seomatic
 * @since     3.0.0
 */

return [
	'*' => [
		'siteType'                     => 'Organization',
		'siteSubType'                  => '',
		'siteSpecificType'             => '',
		'computedType'                 => 'Organization',
		'genericName'                  => 'Our Name is Mud Ltd',
		'genericAlternateName'         => 'Mud',
		'genericDescription'           => '',
		'genericUrl'                   => 'https://ournameismud.co.uk/',
		'genericImage'                 => 'https://mud-assets.imgix.net/media/Facebook.jpg?auto=compress%2Cformat&crop=focalpoint&fit=min&fp-x=0.5&fp-y=0.5&h=400&q=100&w=800',
		'genericImageWidth'            => '800',
		'genericImageHeight'           => '400',
		'genericImageIds'              => [],
		'genericTelephone'             => '',
		'genericEmail'                 => 'hello@ournameismud.co.uk',
		'genericStreetAddress'         => '5 Princes Buildings',
		'genericAddressLocality'       => 'George Street',
		'genericAddressRegion'         => 'NY',
		'genericPostalCode'            => 'BA1 2ED',
		'genericAddressCountry'        => 'GB',
		'genericGeoLatitude'           => '51.385234',
		'genericGeoLongitude'          => '-2.3634617',
		'personGender'                 => '',
		'personBirthPlace'             => '',
		'organizationDuns'             => '',
		'organizationFounder'          => 'Matt Powell, Cole Henley',
		'organizationFoundingDate'     => '2013-05-02',
		'organizationFoundingLocation' => 'Bath, GB',
		'organizationContactPoints'    => [],
		'corporationTickerSymbol'      => '',
		'localBusinessPriceRange'      => '',
		'localBusinessOpeningHours'    => [],
		'restaurantServesCuisine'      => '',
		'restaurantMenuUrl'            => '',
		'restaurantReservationsUrl'    => '',
	],
];
